//go:generate goversioninfo -icon=ac.ico
package main

import (
	"bufio"
	"errors"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/fatih/color"
)

// Define colors used by Altea Console
var textColor *color.Color
var alteaColor *color.Color
var macroColor *color.Color
var alertColor *color.Color

// Define the variable dictionary
var dictionary map[string]string

// Define the Altea session ID
var sessionID string

/*
	The AC (Altea Console) program is a command line program to execute commands on Altea
*/
func main() {

	textColor = color.New(color.FgWhite)
	alteaColor = color.New(color.FgHiCyan)
	macroColor = color.New(color.FgHiGreen)
	alertColor = color.New(color.FgRed)

	textColor.Printf("Welcome to the Altea Console\n")

	initializeDictionary()

	if !initializeSession(dictionary["office"]) {
		return
	}

	textColor.Printf("Use the ? command to get help\n> ")

	scanner := bufio.NewScanner(os.Stdin)
	var exit bool

	for !exit && scanner.Scan() {
		exit = dispatchInput(scanner.Text())
		textColor.Printf("> ")
	}

	textColor.Printf("Goodbye...\n")
}

/*
	Intiliaze the variable dictionary from dictionary.ac
*/
func initializeDictionary() {

	dictionary = make(map[string]string)
	file, err := os.Open("dictionary.ac")

	if err != nil {
		alertColor.Printf("Failed to load dictionary (dictionary.ac): %s\n", err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		line := scanner.Text()

		if line != "" {
			keyValue := strings.Split(line, "=")

			if len(keyValue) == 2 {
				key := strings.ToLower(keyValue[0])
				dictionary[key] = keyValue[1]
			}
		}
	}

	textColor.Printf("Loaded %d variables from dictionary (dictionary.ac)\n", len(dictionary))
}

/*
	Initialize the Altea session
*/
func initializeSession(office string) bool {

	if office == "" {
		office = "AMSKL08BC"
	}

	textColor.Printf("Initialize Altea session with office %s\n", office)

	response, err := executeSoapRequest("http://webservices.amadeus.com/1ASIWCOMKL/VLSTLQ_07_4_1A", alteaSignInRequest(office))

	if err == nil && response != "" {
		regularExpression := regexp.MustCompile("<SessionId>(.*?)</SessionId>")
		sessionID = regularExpression.FindString(response)

		if sessionID != "" {
			sessionID = removePrefixSuffix(sessionID, "<SessionId>", "</SessionId>")
		}
	}

	if sessionID == "" {
		alertColor.Printf("Unable to initialize Altea session\n")
		return false
	}

	textColor.Printf("Using Altea session ID %s\n", sessionID)

	return true
}

/*
	The dispatchInput method will determine if the command has special meaning to the Altea Console
	If so, it will dispatch to the appropriate handler. If not, the command is assumed to be an Altea command
*/
func dispatchInput(input string) bool {

	command := strings.ToUpper(input)
	command = strings.TrimSpace(command)

	if command == "BYE" {
		return true
	} else if command == "?" {
		displayUsage()
	} else if command == "*" {
		displayDictionary()
	} else if command == "**" {
		initializeDictionary()
	} else if command == "!" {
		displayMacros()
	} else if strings.HasPrefix(command, "!") {
		runMacro(input)
	} else if strings.HasPrefix(command, "^") {
		jumpOffice(command)
	} else {
		responseBody := executeCommandScrolling(command)
		alteaColor.Printf("%s\n", responseBody)
	}

	return false
}

/*
	Jump office by initializing an Altea session with the specified office
*/
func jumpOffice(command string) {

	values := strings.Split(command, " ")
	var office string

	if len(values) == 1 {
		office = strings.TrimPrefix(command, "^")
	} else if len(values) == 2 {
		office = strings.TrimSpace(values[1])
	} else {
		alertColor.Printf("Invalid command to jump office\n")
		return
	}

	initializeSession(office)
}

/*
	Execute an Altea command and automatically scroll if necessary
*/
func executeCommandScrolling(command string) string {

	responseBody, err := executeCommandWithRetry(command)

	if err == nil {
		scroll := (strings.HasPrefix(command, "RT") || strings.HasPrefix(command, "IR") || strings.HasPrefix(command, "ER"))

		for scroll && strings.HasSuffix(responseBody, ")") {
			page, err := executeCommandWithRetry("MD")

			if err == nil {
				responseBody = mergePage(responseBody, page)
			}
		}
	}

	return responseBody
}

/*
	Merge two response 'pages' into a combined response
	(skip all lines already present and append the rest)
*/
func mergePage(current, next string) string {

	responseBody := strings.TrimSuffix(current, ")")

	lines := strings.Split(next, "\n")

	for _, line := range lines {
		if !strings.Contains(responseBody, line) {
			responseBody += line
			responseBody += "\n"
		}
	}

	return responseBody
}

/*
	Execute an Altea and retry once if an error occurs
*/
func executeCommandWithRetry(command string) (string, error) {

	response, err := executeCommand(command)

	if err != nil {
		retry := true

		if strings.Contains(err.Error(), "Inactive conversation") {
			retry = initializeSession(dictionary["office"])
		}

		if retry {
			response, err = executeCommand(command)
		}
	}

	return response, err
}

/*
	Execute an Altea command
*/
func executeCommand(command string) (string, error) {

	var responseBody string
	response, err := executeSoapRequest("http://webservices.amadeus.com/1ASIWCOMKL/HSFREQ_07_3_1A", alteaCommandRequest(command))

	if err != nil {
		alertColor.Printf("Failed to execute command: %s\n", err)
		return responseBody, err
	}

	if response != "" {
		bodyExpression := regexp.MustCompile("<textStringDetails>(?s)(.*)</textStringDetails>")
		responseBody = bodyExpression.FindString(response)

		faultExpression := regexp.MustCompile("<faultstring>(.*)</faultstring>")
		responseFault := faultExpression.FindString(response)

		if responseFault != "" {
			responseFault = removePrefixSuffix(responseFault, "<faultstring>", "</faultstring>")
			alertColor.Printf("Failed to execute command: %s\n", responseFault)
			return responseFault, errors.New(responseFault)
		}

		if responseBody != "" {
			responseBody = removePrefixSuffix(responseBody, "<textStringDetails>", "</textStringDetails>")
			responseBody = strings.Replace(responseBody, "\r", "\n", -1)
			responseBody = strings.TrimRight(responseBody, " ")
		}
	}

	return responseBody, err
}

/*
	Execute an Altea SOAP request
*/
func executeSoapRequest(soapAction, requestBody string) (string, error) {

	var responseBody string

	client := &http.Client{}
	httpRequest, err := http.NewRequest("POST", "http://KLaccess.test.webservices.1a.amadeus.com", strings.NewReader(requestBody))

	if err == nil {
		httpRequest.Header.Add("SOAPAction", soapAction)
		httpResponse, err := client.Do(httpRequest)

		if err == nil {
			defer httpResponse.Body.Close()
			bytes, err := ioutil.ReadAll(httpResponse.Body)

			if err == nil {
				responseBody = string(bytes[:])
			}
		}
	}

	return responseBody, err
}

/*
	Run a specific macro
*/
func runMacro(input string) {

	name := strings.TrimPrefix(input, "!")
	name = strings.TrimSpace(name)

	if !strings.HasSuffix(name, ".run") {
		name += ".run"
	}

	macroColor.Printf("Executing macro %s\n", name)

	lines, err := readMacro(name)

	if err != nil {
		alertColor.Printf("Failed to execute macro: %s\n", err)
		return
	}

	promptMacroVariables(lines)
	executeMacro(lines)
}

/*
	Prompt for values of variables and add them to the dictionary
*/
func promptMacroVariables(lines []string) {

	for _, line := range lines {
		if strings.HasPrefix(line, "?") {
			promptMacroVariableExplicit(line)
		} else {
			promptMacroVariablesImplicit(line)
		}
	}
}

/*
	Prompt for the value of a specific variable and add it to the dictionary
*/
func promptMacroVariableExplicit(line string) {

	variable := strings.TrimPrefix(line, "?")
	variable = strings.TrimSpace(variable)

	key := strings.ToLower(variable)
	value := dictionary[key]

	input := promptMacroVariable(variable, value)

	if input != "" {
		dictionary[key] = input
	}
}

/*
	Prompt (if necessary) for a value for all variables and add them to the dictionary
*/
func promptMacroVariablesImplicit(line string) {

	regularExpression := regexp.MustCompile("\\{(.*?)\\}")
	variables := regularExpression.FindAllString(line, -1)

	for _, variable := range variables {
		variable = removePrefixSuffix(variable, "{", "}")
		variable = strings.TrimSpace(variable)

		key := strings.ToLower(variable)
		value := dictionary[key]

		if value == "" {
			input := promptMacroVariable(variable, value)

			if input != "" {
				dictionary[key] = input
			}
		}
	}
}

/*
	Prompt for a value for a variable
*/
func promptMacroVariable(variable, value string) string {

	if value != "" {
		macroColor.Printf("%s (%s)> ", variable, value)
	} else {
		macroColor.Printf("%s> ", variable)
	}

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	return scanner.Text()
}

/*
	Execute the Altea commands, line by line, replacing any variables by values from the dictionary
*/
func executeMacro(lines []string) {

	for _, line := range lines {
		command := line

		if !strings.HasPrefix(line, "?") {
			regularExpression := regexp.MustCompile("\\{(.*?)\\}")
			variables := regularExpression.FindAllString(command, -1)

			for _, variable := range variables {
				key := variable
				key = removePrefixSuffix(key, "{", "}")
				key = strings.TrimSpace(key)
				key = strings.ToLower(key)
				value := dictionary[key]
				command = strings.Replace(command, variable, value, -1)
			}

			command = strings.ToUpper(command)
			command = strings.TrimSpace(command)
			macroColor.Printf("> %s\n", command)
			responseBody, err := executeCommandWithRetry(command)

			if err != nil {
				break
			}

			alteaColor.Printf("%s\n", responseBody)
		}
	}
}

/*
	Read the specified macro file
*/
func readMacro(name string) ([]string, error) {

	file, err := os.Open(name)

	if err != nil {
		return nil, err
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)
	var lines []string

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	return lines, scanner.Err()
}

/*
	Display a list of macro's
*/
func displayMacros() {

	currentDir, currentErr := filepath.Glob("*.run")
	subDir, subErr := filepath.Glob(filepath.FromSlash("*/*.run"))

	if currentErr != nil || subErr != nil {
		alertColor.Printf("Failed to display macros\n")
		return
	}

	var files []string
	files = append(files, currentDir...)
	files = append(files, subDir...)

	for _, file := range files {
		textColor.Printf("%s\n", file)
	}
}

/*
	Display the variable dictionary
*/
func displayDictionary() {

	for key, value := range dictionary {
		textColor.Printf("%s = %s\n", key, value)
	}
}

/*
	Display some useful help information
*/
func displayUsage() {

	textColor.Printf(
		"Usage:\n" +
			"  ?         Display this usage information\n" +
			"  *         Display variable dictionary (dictionary.ac)\n" +
			"  **        Re-load variable dictionary (dictionary.ac)\n" +
			"  !         Display a list of macro's\n" +
			"  ![name]   Execute a macro\n" +
			"  ^         Jump to default office (office defined in dictionary.ac or AMSKL08BC)\n" +
			"  ^[office] Jump to office\n" +
			"  [command] Execute the Altea command\n" +
			"  bye       Exit Altea Console\n" +
			"  \n" +
			"Macro:\n" +
			"  Every line, except a line starting with ?, is an Altea command\n" +
			"  A line starting with ? will explicitly prompt for a variable value\n" +
			"  Use {name} to insert a variable value\n" +
			"  {name} will prompt for a variable value if a value is not in the dictionary yet\n" +
			"  \n" +
			"Dictionary:\n" +
			"  Use dictionary.ac to configure pre-defined variables\n" +
			"  Every line defines the name and value of a variable, separated by =" +
			"  \n")
}

/*
	Construct the Altea sign in request
*/
func alteaSignInRequest(office string) string {

	request := "<?xml version=\"1.0\"?>\n" +
		"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wbs=\"http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd\" xmlns:vls=\"http://xml.amadeus.com/VLSTLQ_07_4_1A\">\n" +
		"<soapenv:Body>\n" +
		"<vls:Security_TrustedUserSignIn xmlns=\"http://xml.amadeus.com/VLSTLQ_07_4_1A\">\n" +
		"<vls:userIdentifier>\n" +
		"<vls:originIdentification>\n" +
		"<vls:inHouseIdentification1>{office}</vls:inHouseIdentification1>\n" +
		"</vls:originIdentification>\n" +
		"<vls:originatorTypeCode>U</vls:originatorTypeCode>\n" +
		"<vls:originator>WSKLACC</vls:originator>\n" +
		"</vls:userIdentifier>\n" +
		"<vls:userIdentifier>\n" +
		"<vls:originIdentification>\n" +
		"<vls:inHouseIdentification1>{office}</vls:inHouseIdentification1>\n" +
		"</vls:originIdentification>\n" +
		"<vls:originatorTypeCode>Z</vls:originatorTypeCode>\n" +
		"<vls:originator>9998WS</vls:originator>\n" +
		"</vls:userIdentifier>\n" +
		"<vls:dutyCode>\n" +
		"<vls:referenceDetails>\n" +
		"<vls:type>DUT</vls:type>\n" +
		"<vls:value>SU</vls:value>\n" +
		"</vls:referenceDetails>\n" +
		"</vls:dutyCode>\n" +
		"<vls:systemDetails>\n" +
		"<vls:deliveringSystem>\n" +
		"<vls:companyId>KL</vls:companyId>\n" +
		"</vls:deliveringSystem>\n" +
		"</vls:systemDetails>\n" +
		"</vls:Security_TrustedUserSignIn>\n" +
		"</soapenv:Body>\n" +
		"<wbs:Session>\n" +
		"<wbs:SessionId/>\n" +
		"<wbs:SequenceNumber/>\n" +
		"<wbs:SecurityToken/>\n" +
		"</wbs:Session>\n" +
		"</soapenv:Envelope>\n"

	request = strings.Replace(request, "{office}", office, -1)

	return request
}

/*
	Construct an Altea command request
*/
func alteaCommandRequest(command string) string {

	request := "<?xml version=\"1.0\"?>\n" +
		"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wbs=\"http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd\" xmlns:hsf=\"http://xml.amadeus.com/HSFREQ_07_3_1A\">\n" +
		"<soapenv:Header>\n" +
		"<wbs:SessionId>{sessionID}</wbs:SessionId>\n" +
		"</soapenv:Header>\n" +
		"<soapenv:Body>\n" +
		"<hsf:Command_Cryptic>\n" +
		"<hsf:messageAction>\n" +
		"<hsf:messageFunctionDetails>\n" +
		"<hsf:messageFunction>1</hsf:messageFunction>\n" +
		"</hsf:messageFunctionDetails>\n" +
		"</hsf:messageAction>\n" +
		"<hsf:longTextString>\n" +
		"<hsf:textStringDetails>{command}</hsf:textStringDetails>\n" +
		"</hsf:longTextString>\n" +
		"</hsf:Command_Cryptic>\n" +
		"</soapenv:Body>\n" +
		"</soapenv:Envelope>\n"

	request = strings.Replace(request, "{sessionID}", sessionID, -1)
	request = strings.Replace(request, "{command}", command, -1)

	return request
}

/*
	Remove the specified prefix and suffix from the value
*/
func removePrefixSuffix(value, prefix, suffix string) string {

	result := strings.TrimPrefix(value, prefix)
	result = strings.TrimSuffix(result, suffix)

	return result
}
